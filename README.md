# Probe Design

We want to design an RNA probe to target environmental DNA from the Tursiops truncatus.

See more details on the [wiki](https://gitlab.mbb.univ-montp2.fr/bev/dolphin_hyrad/dolphin_enzyme/-/wikis/Probe-Design)

## Author

Created by Morgane Bruno and Laureline Boulanger.

Credits to Pierre Edouard Guérin.

## License

The code is available under the MIT license. See the LICENSE file for more info.
