#!/bin/bash

genome=$1    # uncompress fasta
pairs_enz=$2 # pairs_restriction_enzymes.txt

# Compress genome
[-f "$genome" ] && bgzip $genome

# index fasta
samtools faidx $genome'.gz'
# Create a chorm.list comprising the first three linkage groups from index (fai)
fai=$(echo $genome'.gz'| sed 's/$/\.fai/')
cat $fai | cut -f 1 > data/chrom.list

# ddrad, size selection 300+-50
while read p
do 
	enz_1=$(echo $p | awk '{print $1}')
	enz_2=$(echo $p | awk '{print $2}')
	echo 'info:: running radinitio with '$enz_1 '&' $enz_2
	out_dir='result/loci_ddrad_'$enz_1'_'$enz_2'/'
	mkdir -p $out_dir
	radinitio --tally-rad-loci --genome $genome'.gz' --chromosomes data/chrom.list \
		--out-dir $out_dir --library-type ddRAD \
		--enz $enz_1 --enz2 $enz_2 --insert-mean 300 --insert-stdev 50
done < $pairs_enz

